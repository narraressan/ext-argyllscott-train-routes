### Crea's Train Routes App

an examination project with the goal to write a command line application using Python with the following requirements:

```bash
# simply run the following commands
python main.py --file=routes.csv

# or
python main.py --file routes.csv

# and, to run the test script
python test.py
```

##### Acceptance Criteria:
- When run in a terminal, the app takes a `routes.csv` file and stores route information in
memory

- App interface:
    - The user inputs a single starting train station
    - he user inputs a single ending train station
    - The resulting output should display number of stops and length of trip

- Utilize object oriented patterns

- Write code that is production ready

- Include unit tests

- `NO` usage of any external libraries

##### Input format

- Example `routes.csv` format:

    ```
    A,B,5
    B,C,5
    C,D,7
    A,D,15
    E,F,5
    F,G,5
    G,H,10
    H,I,10
    I,J,5
    G,J,20
    ```

- Each line includes:
    - A starting station name
    - An ending station name
    - The time it takes to get from the starting station to the ending station

- Example command line interface:

    ```bash
    python main.py --file=routes.csv

    # user inputs A and D
    $ what station are you getting on the train?:A
    $ what station are your getting off the train?:D

    # script responds with...
    $ your trip from A to D will include 0 stops and will take 15 minutes
    ```

- Example inputs and result:
    - Scenario 1

        ```
        A
        B
        Result: 0 stops, 5 minutes
        ```
    - Scenario 2

        ```
        A
        C
        Result: 1 stop, 10 minutes
        ```

    - Scenario 3

        ```
        E
        J
        Result: 4 stops, 30 minutes
        ```

    - Scenario 4

        ```
        A
        D
        Result: 0 stops, 15 minutes
        ```

    - Scenario 4

        ```
        A
        J
        Result: No routes from A to J
        ```