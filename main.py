
from utils import utils

if __name__ == '__main__':

    file = utils.parse_command()

    if file != None:

        route_map = utils.load_map(file)

        if route_map:
            # ask user for which station to get on and off
            start = utils.ask_for_station('\nWhat station are you getting on the train?:', route_map)
            end = utils.ask_for_station('\nwhat station are your getting off the train?:', route_map)

            # calculate shortest train route
            if route_map.set_valid_destinations(start, end):
                time = route_map.find_shortest_route()
                path = route_map.build_path_history()

                # print(time, path) # you can print path to see the history of stations travelled

                utils.print_results(start, end, time, path)