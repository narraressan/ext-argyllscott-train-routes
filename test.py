
"""
simply run with:
    python test.py
"""

from random import randint
from utils import utils

if __name__ == '__main__':

    # test command line parser
    file_name = 'routes.csv'
    file1 = utils.parse_command(['--file=%s'%(file_name)])
    print('file1:\t', file1)
    assert file1 == file_name

    file2 = utils.parse_command(['--file', '%s'%(file_name)])
    print('file2:\t', file2)
    assert file2 == file_name


    # auto generate csv to test Map
    csv_data = []
    stations = 'ABCDEFGHIJKLMPOPQRSTUVWXYZ'
    test_cases = 50

    for nth in range(0, test_cases):
        for start in stations:
            for end in stations:
                # randomize point to point destinataion
                csv_data.append([start, end, str(randint(0, 99999))])

        route_map = utils.load_data(csv_data)

        start = stations[randint(0, len(stations)-1)]
        end = stations[randint(0, len(stations)-1)]

        if route_map.set_valid_destinations(start, end):
            time = route_map.find_shortest_route()
            path = route_map.build_path_history()

            # print(time, path)
            utils.print_results(start, end, time, path)

    print('\n\nTest successful. No error raised')