
import math

"""
calculates the nearest route based on start and end points from user
uses list of routes (route classes) as reference
"""

class Map:

    known_routes = {}
    start = None
    end = None
    shortest_route = {}
    passed_station = {}
    noRoute = math.inf


    def __init__(self, routes={}):
        self.known_routes = routes


    def add_route(self, start, end, time):
        """
        add a new route to the map where
        key is a unique station name and
        value is a dictionary of all it's known next stop

        ex.
            known_routes = {
                station1: {
                    station2: 10,
                    station3: 10
                }
                station2: { ... },
                ...
            }
        """
        if start not in self.known_routes:
            self.known_routes[start] = {}

        if end not in self.known_routes:
            self.known_routes[end] = {}

        self.known_routes[start][end] = time


    def check_if_station_exists(self, station):
        # check if this station exists from the known_routes

        if station in self.known_routes:
            return True

        return False


    def get_routes(self):
        return self.known_routes


    def set_valid_destinations(self, start, end):
        if start == end:
            print('\nPlease provide a different start (%s) and end (%s) stations' % (start, end))
            return False
        else:
            self.start = start
            self.end = end

            # reset the calculated paths for new values
            self.shortest_route = {}
            self.passed_station = {}

        return True


    def find_shortest_route(self):
        # core concepet follows Dijkstra's algorithm

        unvisited_routes = self.known_routes

        """
        first, set all stations with noRoute value to
        defined that it hasn't been reach or it is not reachable
        """
        for station in unvisited_routes:
            self.shortest_route[station] = self.noRoute

        # secondly, set 'start' as 0 since train hasn't moved
        self.shortest_route[self.start] = 0

        # traverse all unvisited routes
        while unvisited_routes:
            closest_station = None

            for station in unvisited_routes:
                if closest_station == None or self.shortest_route[station] < self.shortest_route[closest_station]:
                    closest_station = station

            # concept adapted from Dijkstra's algorithm
            for end_station, time in self.known_routes[closest_station].items():
                tmp_route_total = time + self.shortest_route[closest_station]

                if tmp_route_total < self.shortest_route[end_station]:
                    self.shortest_route[end_station] = tmp_route_total
                    self.passed_station[end_station] = closest_station

            unvisited_routes.pop(closest_station)

        if self.end in self.shortest_route and self.shortest_route[self.end] != math.inf:
            return self.shortest_route[self.end]

        return None


    def build_path_history(self):
        # build the history of travel by back tracking on the stations passess

        path = []
        current_station = self.end

        while current_station != self.start:
            try:
                path.insert(0, current_station)
                current_station = self.passed_station[current_station]
            except KeyError:
                print('\nResult: No routes from %s to %s'%(self.start, self.end))
                break

        path.insert(0, self.start)

        return path
