
import sys, csv, math
from .map import Map


"""
accept args where
    '--file' -> is the csv file location
"""
def parse_command(tmp_args=None):
    file_arg = '--file'
    file_dir = None
    args = tmp_args or sys.argv[0:]

    known_errors = {
      'no_file': 'No file provided during execution.',
      'unknown': 'Unknown exception occured.',
      'help': """\nno valid args detected:\n\n--file\t\tto specify file location | ex. --file=routes.csv\n\n"""
    }

    try:
      if file_arg in args:

          try:
            file_dir = args[ args.index(file_arg) + 1 ]

          except IndexError as no_file_err:
            print(known_errors['no_file'])

      else:

          for arg in args:
            tmp_name = file_arg + '='

            if tmp_name in arg:
                tmp_dir = arg.split(tmp_name)

                if len(tmp_dir) < 2:
                  print(known_errors['no_file'])
                else:
                  file_dir = tmp_dir[1]

      if file_dir != None and file_dir.strip() == '':
          print(known_errors['no_file'])

      elif file_dir == None:
          print(known_errors['help'])

    except Exception as err:
      print(known_errors['unknown'])

    return file_dir


"""
ask user for a station and
if station is unknown ask again
"""
def ask_for_station(message, map_route):
    while True:
        station = input(message)

        if map_route.check_if_station_exists(station):
            return station
        else:
            print('\nSorry, this Station does not exist. Please try again.\n')


# parse the csv here
def load_map(file_dir):
    try:
      with open(file_dir) as csv_file:
          csv_data = csv.reader(csv_file, delimiter=',')

          return load_data(csv_data)

    except Exception as err:
      print(err)

    return None


# load each csv row to Map
def load_data(csv_data=[]):
    routes = Map()

    for row in csv_data:
        # a valid row must contain at least 3 elements (start, end, time)
        if len(row) >= 3:
            try:
                start = row[0].strip()
                end = row[1].strip()
                time = int(row[2].strip())

                """
                assume that if start and end are the same, it means train didn't move
                and time should always be larger than 0
                """
                if start != end and time > 0 and time < math.inf:
                    routes.add_route(start, end, time)

            except Exception as err:
                # skip this loop since parsing failed
                # print(err)
                pass

    return routes


def print_results(start, end, time, path):
    if path != None and time != None:
        print('\n')
        print(start)
        print(end)

        # the first and last index are the start and endpoints
        stops = len(path)-2

        print('Result: %s stop/s, %s minutes (%s hrs)' % (str(stops), str(time), str(time/60)))
